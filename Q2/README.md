# Requirements
- The EC2 instances should be running with pulbic ips and port 22 is allowed to access
- `ec2-user` is the account for ssh access and same key pair is installed in EC2 Instances
- The key pair is set as default in local machine which runs the script
- The IAM permission of AWS Access Key should have necessary IAM permission for `EC2:DescribeInstance` and `EC2:DescribeRegion`

# Usage
1. Replace `<AWS_ACCESS_KEY>` with your own AWS Access Key in the script
2. Replace `<AWS_SECRET_KEY>` with your own AWS Secret Key in the script
3. Run the script
```bash
awsssh.sh <EC2 Name tag>
```

# Limitation
- The script will search all instances in all avaialble regions in ascending order. It only finds the first matched one for ssh access


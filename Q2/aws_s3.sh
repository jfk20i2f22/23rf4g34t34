#!/bin/bash

if [ $# -eq 0 ]
then 
    echo "Usage: awsssh <EC2 Name Tag>"
    exit 1
fi

function hmac_sha256 {
    key="$1"
    data="$2"
    echo -n "$data" | openssl dgst -sha256 -mac HMAC -macopt "$key" | sed 's/^.* //'
  }

function callAwsEc2Api(){
  #Initialize variables for API call
  AWSACCESSKEY=$1
  AWSSECRETKEY=$2
  AWSSERVICE="ec2"
  AWSREGION=$3
  HTTPMETHOD="GET"
  CANONICALURI="/"
  CANONICALQUERYSTRING=$4
  CANONICALPOSTDATA=''
  CANONICALHASHPOSTDATA=$(echo -n $CANONICALPOSTDATA | openssl dgst -sha256 | awk -F ' ' '{print $2}')
  CDATETIME=$(date -u +"%Y%m%dT%H%M%SZ")
  CDATE=$(date -u +"%Y%m%d")

  #Create a canonical request for Signature Version 4
cat > ./creq << EOF
$HTTPMETHOD
$CANONICALURI
$CANONICALQUERYSTRING
content-type:application/json
host:$AWSSERVICE.$AWSREGION.amazonaws.com
x-amz-content-sha256:$CANONICALHASHPOSTDATA
x-amz-date:$CDATETIME

content-type;host;x-amz-content-sha256;x-amz-date
$CANONICALHASHPOSTDATA
EOF

  printf %s "$(cat creq)" > creq
  CANONICALREQUESTHASH=$(openssl dgst -sha256 ./creq | awk -F ' ' '{print $2}')

  #Create a string to sign for Signature Version 4
cat > ./stringtosign << EOF
AWS4-HMAC-SHA256
$CDATETIME
$CDATE/$AWSREGION/$AWSSERVICE/aws4_request
$CANONICALREQUESTHASH
EOF

  printf %s "$(cat stringtosign)" > stringtosign

  #Calculate the signature for AWS Signature Version 4
  DATEKEY=$(hmac_sha256 key:"AWS4$AWSSECRETKEY" $CDATE)
  DATEREGIONKEY=$(hmac_sha256 hexkey:$DATEKEY $AWSREGION)
  DATEREGIONSERVICEKEY=$(hmac_sha256 hexkey:$DATEREGIONKEY $AWSSERVICE)
  HEXKEY=$(hmac_sha256 hexkey:$DATEREGIONSERVICEKEY "aws4_request")

  SIGNATURE=$(openssl dgst -sha256 -mac HMAC -macopt hexkey:$HEXKEY stringtosign | awk -F ' ' '{print $2}')

  # curl -vvv "https://$AWSSERVICE.$AWSREGION.amazonaws.com/?Action=DescribeInstances&Filter.1.Name=tag:Name&Filter.1.Value.1=$1" \
  curl -s "https://$AWSSERVICE.$AWSREGION.amazonaws.com/?$CANONICALQUERYSTRING" \
      -X  $HTTPMETHOD \
      -H  "Authorization: AWS4-HMAC-SHA256 Credential=$AWSACCESSKEY/$CDATE/$AWSREGION/$AWSSERVICE/aws4_request, SignedHeaders=content-type;host;x-amz-content-sha256;x-amz-date, Signature=$SIGNATURE" \
      -H "x-amz-date:$CDATETIME" \
      -H "content-type:application/json" \
      -H "x-amz-content-sha256:$CANONICALHASHPOSTDATA"
  
  rm -f stringtosign creq

}

#Initialize variables for API call
AWSACCESSKEY=<AWS_ACCESS_KEY>
AWSSECRETKEY=<AWS_SECRET_KEY>

REGIONLIST=`callAwsEc2Api $AWSACCESSKEY $AWSSECRETKEY us-east-1 "Action=DescribeRegions&Version=2016-11-15" | grep -oP "(?<=<regionName>)[^<]+" | sort `

for i in $REGIONLIST
do
  HOSTIP=`callAwsEc2Api $AWSACCESSKEY $AWSSECRETKEY $i "Action=DescribeInstances&Filter.1.Name=tag%3AName&Filter.1.Value.1=$1&Version=2016-11-15" | grep -oPm1 "(?<=<publicIp>)[^<]+"`
  if [[ ! -z "$HOSTIP" ]]
  then
    echo "ssh ec2-user@$HOSTIP"
    bash -c 'ssh ec2-user@'$HOSTIP
    exit 0
  fi
done

echo "HOST not found"
# System diagram

![Shorten URL service](Shorten_URL_Service.png)  

- The above diagram showed the design of shorten URL service and it aims to achieve the following:  
- High availability  
    - Both application load balancer and dynamodb is available at least 99.99%. They are fully managed AWS service.  
    - There are at least 2 EC2 instance in the auto scaling group and they are hosted in different available zones to provide high availability. When unheathly of EC2 instance is reported by application load balancer or auto scaling group, the replacement of EC2 will be triggered immediately  
- Scalability  
    - Application load balancer automatically scales in or out of LCU based on the traffic  
    - Auto scaling group manages the number of EC2 instance based on the count of request to application load balancer and the CPU utilization of EC2 instance  
    - Write and Read unit of DynamoDB is scaled in or our based on the capacity utilization  
- Scaling target: 1000+ req/s  
    - The LCU of Application load balancer can be scaled out based on the count of request  
    - Each EC2 instance can process 100 request/s and the number of EC2 instance can be scaled out up to 20 to handle 1000- 2000 request/s. The max number of EC2 instance can be modified if necessary  
    - The write and read unit of DynamoDB can be scaled out up to 2000 units individually. It can afford about 2000 request/s  

- Since the application is required to host in genic EC2 instance, auto scaling group are used to provide high availability and scalabilty. Launch configuration of auto scaling group provides ease deployment of EC2. s
- Application load balancer can provide single point of contact for users and disributes the requests across multiple targets.
- Only `URL` and `shortened ID` are requied to store in database and only `shortened ID` is requried to query. DynamoDB is enough for this. It costs based on the usage and storage instead of the uptime of resource. 

# Requirements

- Create a IAM user with Administrator access.
- Generate the access key and secret key for the IAM user  
- Install AWS CLI and setup it with the access key and secret key
- The cloudformation template can be run in `us-east-1`, `us-west-1`, `eu-west-1`, `ap-southeast-1` and `ap-northeast-1` region


# Deployment

- To deploy the shorten URL service in AWS environment (e.g. in region `ap-southeast-1`)
```bash
aws cloudformation deploy --template-file aws/shorturl.yml \
--capabilities CAPABILITY_NAMED_IAM \
--stack-name shorturl \
--region ap-southeast-1
```

# Get the endpoint of sevice

- After the successful deployment, get the endpoint of service with the below command (e.g. in region `ap-southeast-1`)
```bash 
aws cloudformation describe-stacks --stack-name shorturl --region ap-southeast-1 | grep -A 1 shorturlendpoint
```
- The value of `OutputValue` is the endpoint of service
```
                    "OutputKey": "shorturlendpoint",
                    "OutputValue": "shorturl-prod-shorturl-835047251.ap-southeast-1.elb.amazonaws.com",
```

# Usage

- Shorten URL
```bash
curl --request POST 'http://shorturl-prod-shorturl-835047251.ap-southeast-1.elb.amazonaws.com/newurl' \
--header 'Content-Type: application/json' \
--data-raw '{ "url": "https://www.google.com" }'
```

- Redirect
```bash
curl --request GET 'https://shorturl-prod-shorturl-1207199159.ap-southeast-1.elb.amazonaws.com/brr9cucST'
```

# Removal

- Remove the cloudformation stack (e.g. in region `ap-southeast-1`)
```bash
aws cloudformation delete-stack --stack-name shorturl --region ap-southeast-1
```

# Limitatiosn

- HTTP is used for the service instead of HTTPS since no domain and SSL certificate is purchasd
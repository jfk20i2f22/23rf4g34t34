const dynamoose = require('dynamoose')

const urldataschema = new dynamoose.Schema(
    {
        sid: {
            type: String,
            hashKey: true,
            required: true
        },
        url: {
            type: String,
            required: true
        }
    },
    {
        timestamps: true,
    }    
);

const urldatamodel = dynamoose.model('shortenurl', urldataschema)

module.exports = urldatamodel
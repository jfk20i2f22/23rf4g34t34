const shorturl = require('../models/shorturl');
const { nanoid } = require('nanoid');
const yup = require('yup');

const newUrlSchema = yup.object().shape({
    url: yup.string().trim().url().required()
});

const redirect = async (req, res) => {
    const{surl:sid}=req.params;
    try {
        const url = await shorturl.query("sid").eq(sid).exec();
        if (url.count != 0) {
            return res.redirect(url[0]['url']);
        }
        else {
            return res.status(404).send({message:'invalid url'});
        }
    }catch(error) {
        return res.status(404).send({message:'invalid url'});
    }
}

const store = async ( req, res, next ) => {
    let { url } = req.body;
    try {
        await newUrlSchema.validate({url});
        var sid = nanoid(9);
        while ( sid.includes('-') || sid.includes('_') || ((await shorturl.query("sid").eq(sid).count().exec())['count']) != 0 ){
            sid = nanoid(9);
        }
        const newurldata = new shorturl({"sid":sid,"url":url.trim()})
        await newurldata.save({"overwrite":false});
        res.json({"url":url,"shortenUrl":`${req.protocol}://${req.hostname}/${sid}`});
    } catch (error) {
        next(error);
    }
}

module.exports = { redirect,store }
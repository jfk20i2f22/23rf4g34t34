const dynamoose = require('dynamoose');

require('dotenv').config();

if (process.env.AWSACCESSKEY){
    dynamoose.aws.sdk.config.update({
        "region": process.env.AWSREGION,
        "accessKeyId": process.env.AWSACCESSKEY,
        "secretAccessKey": process.env.AWSSECRETKEY
    });
}
else {
    dynamoose.aws.sdk.config.update({
        "region": process.env.AWSREGION
});
}



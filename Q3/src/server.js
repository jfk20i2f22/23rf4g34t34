const express = require('express');
const app = express();
const routes = require('./routes');
require('./database');

app.use(express.json());
app.use(routes);

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
    console.log(`Server started at ${PORT}`);
});
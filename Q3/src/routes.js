const router = require('express').Router();

const shortrurlcontroller = require('./controllers/shorturlcontroller');

router.get('/', (req,res) => {
    res.send('This is URL-Shortener')
})

router.get('/:surl([a-zA-Z0-9]{9})', shortrurlcontroller.redirect);

router.post('/newurl', shortrurlcontroller.store);

module.exports = router;
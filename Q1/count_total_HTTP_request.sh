#!/bin/bash

#variables
LOGFILE="access.log"

#Count total number of HTTP requests
TOTAL=`awk -F'"' '{print $2}' $LOGFILE | grep HTTP/ | wc -l`

echo "Total number of HTTP requests:" $TOTAL
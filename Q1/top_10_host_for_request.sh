#!/bin/bash

#variables
LOGFILE="access.log"

echo 'Here is Top 10 making most request'
echo 'Count   Host'

#top 10 hosts make most request
cat $LOGFILE | grep 1[0-9]/Jun/2019 | awk  '{print $1}' access.log  |  sort | uniq -c | sort -nr | head -n 10
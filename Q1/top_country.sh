#!/bin/bash

#variables
LOGFILE="access.log"

#Find out all unique IPs
COUNT_IP=`awk  '{print $1}' $LOGFILE |  sort | uniq | sort -nr `

#Revert all IPs of requests
TMP=`awk  '{print $1}' $LOGFILE |  sort`

#Convert the IP to country using whois
for i in $COUNT_IP
do
    if [[ $i =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]
    then
        COUNTRY=`geoiplookup $i | awk -F"," '{print $2}'`
    else
        COUNTRY=`geoiplookup6 $i | awk -F"," '{print $2}'`
    fi
    TMP=${TMP//$i/$COUNTRY}
done

#Store the result in tempoary file and find out the result
echo 'Here is country with most requests originating from'
echo 'Count   Host'
echo "$TMP" | sort | uniq -c | sort -nr | head -n 1
# Requirements
- The scripts is written to run in Amazon Linux 2.

# Usage
- Count the total number of HTTP requests recorded by this access logfile
```bash
./count_total_HTTP_request.sh
```
- Find the top-10 (host) hosts makes most requests from 2019-06-10 00:00:00 to 2019-06-19 23:59:59, inclusively
```bash
./top_10_host_for_request.sh
```
- Find out the country with most requests originating from (according to the source IP)
```bash
./top_country.sh
```

# Limitation
- `top_country.sh` uses `geoiplookup` and `geoiplookup6` for the conversion from IP to country. Its local database may be out-of-database. To update its database, please visit [this](https://dev.maxmind.com/geoip/geoipupdate/)


